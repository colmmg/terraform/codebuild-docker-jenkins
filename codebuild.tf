data "aws_iam_policy_document" "codebuild" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["codebuild.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "codebuild" {
  assume_role_policy = data.aws_iam_policy_document.codebuild.json
  name               = "codebuild-docker-service-role"
  path               = "/service-role/"
  tags = {
    Name = "codebuild-docker-service-role"
  }
}

data "template_file" "codebuild" {
  template = file("${path.module}/templates/codebuild-policy.json.tpl")
  vars = {
    aws_account_id             = data.aws_caller_identity.current.account_id
    codebuild-artifacts-bucket = aws_s3_bucket.codebuild-artifacts.id
    region                     = var.region
  }
}

resource "aws_iam_role_policy" "codebuild" {
  name   = "codebuild-docker"
  policy = data.template_file.codebuild.rendered
  role   = aws_iam_role.codebuild.id
}

resource "aws_codebuild_project" "docker" {
  artifacts {
    type = "NO_ARTIFACTS"
  }
  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    environment_variable {
      name = "AWS_ACCOUNT_ID"
      value = data.aws_caller_identity.current.account_id
    }
    image                       = "aws/codebuild/amazonlinux2-x86_64-standard:2.0"
    privileged_mode             = "true"
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"
  }
  logs_config {
    cloudwatch_logs {
      group_name = "/aws/codebuild/docker"
    }
  }
  name          = "docker"
  service_role  = aws_iam_role.codebuild.arn
  source {
    type            = "S3"
    location        = "${aws_s3_bucket.codebuild-artifacts.id}/docker"
  }
  tags = {
    Name = "docker"
  }
}
