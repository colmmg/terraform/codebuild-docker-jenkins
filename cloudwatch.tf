resource "aws_cloudwatch_log_group" "docker" {
  name = "/aws/codebuild/docker"
  tags = {
    Name = "/aws/codebuild/docker"
  }
}
