{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Effect":"Allow",
      "Resource":[
        "arn:aws:logs:${region}:${aws_account_id}:log-group:/aws/codebuild/docker",
        "arn:aws:logs:${region}:${aws_account_id}:log-group:/aws/codebuild/docker:*"
      ],
      "Action":[
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ]
    },
    {
      "Effect":"Allow",
      "Resource":[
        "arn:aws:s3:::${codebuild-artifacts-bucket}",
        "arn:aws:s3:::${codebuild-artifacts-bucket}/*"
      ],
      "Action":[
        "s3:PutObject",
        "s3:GetObject",
        "s3:GetObjectVersion",
        "s3:GetBucketAcl",
        "s3:GetBucketLocation"
      ]
    },
    {
      "Effect":"Allow",
      "Action":[
        "codebuild:CreateReportGroup",
        "codebuild:CreateReport",
        "codebuild:UpdateReport",
        "codebuild:BatchPutTestCases"
      ],
      "Resource":[
        "arn:aws:codebuild:${region}:${aws_account_id}:report-group/docker-*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "ecr:GetAuthorizationToken"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "ecr:BatchCheckLayerAvailability", 
        "ecr:GetDownloadUrlForLayer", 
        "ecr:GetRepositoryPolicy", 
        "ecr:DescribeRepositories", 
        "ecr:ListImages", 
        "ecr:DescribeImages", 
        "ecr:BatchGetImage", 
        "ecr:InitiateLayerUpload", 
        "ecr:UploadLayerPart", 
        "ecr:CompleteLayerUpload", 
        "ecr:PutImage"
      ],
      "Resource": "arn:aws:ecr:${region}:${aws_account_id}:repository/chef"
    }
  ]
}
