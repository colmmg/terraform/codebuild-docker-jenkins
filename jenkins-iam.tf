resource "aws_iam_user" "jenkins" {
  name = "jenkins"
  tags = {
    Name = "jenkins"
  }
}

resource "aws_iam_access_key" "jenkins" {
  user = "${aws_iam_user.jenkins.name}"
}

data "template_file" "jenkins" {
  template = file("${path.module}/templates/jenkins-policy.json.tpl")
  vars = {
    aws_account_id             = data.aws_caller_identity.current.account_id
    codebuild-artifacts-bucket = aws_s3_bucket.codebuild-artifacts.id
    region                     = var.region
  }
}

resource "aws_iam_user_policy" "jenkins" {
  name   = "jenkins"
  policy = data.template_file.jenkins.rendered
  user   = "${aws_iam_user.jenkins.name}"
}
