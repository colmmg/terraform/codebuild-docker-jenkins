# codebuild-docker-jenkins
This repository contains [Terraform](https://www.terraform.io/) code for deploying the resources needed to configure a [Docker](https://www.docker.com/) build project in [AWS CodeBuild](https://aws.amazon.com/codebuild/) which can be invoked from a [Jenkins](https://jenkins.io/) job.

This repository can be used together with [colmmg/aws-codebuild/docker](https://gitlab.com/colmmg/aws-codebuild/docker).

# Terraform Deploy Instructions
To deploy run:
```
terraform init
terraform apply
```
